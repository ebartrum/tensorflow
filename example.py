import tensorflow as tf

a = tf.placeholder(tf.int64, shape=[], name='a')
b = tf.placeholder(tf.int64, shape=[], name='b')
c = a+b
print(c)

sess = tf.Session()
res = sess.run(c, feed_dict={a: 5, b: 3})
print(res)
