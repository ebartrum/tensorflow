# Example of using an optimizer

import tensorflow as tf
import datetime

x = tf.Variable(1.0,name="x")
y = -x**2 + 4*x - 3
alpha = 10

optimizer = tf.train.AdamOptimizer(learning_rate = alpha).minimize(-y)
num_epochs = 1000
tf.summary.scalar("y", y)

init = tf.global_variables_initializer()
print("Starting...")
with tf.Session() as sess:
    summaryMerged = tf.summary.merge_all()
    filename="./summary_log/run"+datetime.datetime.now().strftime("%Y-%m-%d--%H-%M-%s")
    writer = tf.summary.FileWriter(filename, sess.graph)
    sess.run(init)
    for epoch in range(num_epochs):
        print("Epoch: %d" %epoch)
        _,current_x,current_y,sumOut = sess.run([optimizer,x,y,summaryMerged])
        print("x is %f, y is %f" %(current_x,current_y))
        writer.add_summary(sumOut,epoch)
    writer.close()
